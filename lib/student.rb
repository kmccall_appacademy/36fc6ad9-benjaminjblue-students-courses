class Student
  attr_accessor :first_name, :last_name, :courses
  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def courses
    @courses
  end

  def enroll(course)
    if !self.courses.any?{|enrolled_course| enrolled_course.conflicts_with?(course)}
      if !already_enrolled?(self, course)
        @courses.push(course)
        course.students.push(self)
      end
    else
      raise "#{course.name} conflicts with a course you are already enrolled in."
    end
  end

  def course_load
    hash = Hash.new(0)
    self.courses.each do |course|
      hash[course.department] += course.credits
    end
    hash
  end

  private

  def already_enrolled?(student, course)
    student.courses.any? do |enrolled_course|
      course == enrolled_course
    end
  end

end
